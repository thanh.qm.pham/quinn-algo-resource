public static void qs (int[] arr) {
	qs(arr, 0, arr.length-1);
}

public static void qs (int[] arr, int start, int end) {
	if (start >= end) return;
	
	int pivot = getPivot(arr, start, end);
	int pos = partition(arr, start, end, pivot);
	
	qs(arr, start, pos - 1);
	qs(arr, pos + 1, end);
}

// It's better to randomize an index between start and end.
// I return end index for simplicity
public static int getPivot(int[] arr, int start, int end) {
	return end;
}

public static int partition(int[] arr, int start, int end, int pivot) {
	int val = arr[pivot];
	swap(arr, pivot, end);
	int smallIndex = start;
	for (int i = start; i < end; i++) {
		if (arr[i] < val) {
			swap(arr, i, smallIndex);
			smallIndex++;
		}
	}
	swap(arr, smallIndex, end);
	return smallIndex;
}

public static void swap(int[] arr, int i, int j) {
	int temp = arr[i];
	arr[i] = arr[j];
	arr[j] = temp;
}