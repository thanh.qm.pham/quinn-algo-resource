public static void mergeSort(int[] arr) {
	mergeSort(arr, 0, arr.length-1);
}
	
public static void mergeSort(int[] arr, int start, int end) {
	if (start >= end) return;
	
	int mid = start + (end-start)/2;
	
	mergeSort(arr, start, mid);
	mergeSort(arr, mid+1, end);
	
	merge(arr, start, mid , end);
	
}

public static void merge(int[] arr, int start, int mid, int end) {
	int[] temp = new int[end-start+1];
	int i = start;
	int j = mid+1;
	int curr = 0;
	
	while (i <= mid && j <= end) {
		if (arr[i] <= arr[j]) {
			temp[curr] = arr[i];
			i++;
		} else {
			temp[curr] = arr[j];
			j++;
		}
		curr++;
	}
	
	while (i <= mid) {
		temp[curr] = arr[i];
		curr++;
		i++;
	}
	
	while (j <= end) {
		temp[curr] = arr[j];
		curr++;
		j++;
	}
	
	for (int k = start; k <= end; k++) {
		arr[k] = temp[k-start];
	}
}