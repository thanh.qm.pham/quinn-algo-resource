public static void heapSort(int[] arr) {
	int size = 0;
	for (int i = 1; i < arr.length; i++) {
		heapifyUp(arr, i);
		size++;
	}
			
	while (size > 0) {
		swap(arr, 1, size);
		size--;
		heapifyDown(arr, size, 1);
	}
}

public static void heapifyUp(int[] arr, int curr) {
	while (curr/2 >= 1 && arr[curr] > arr[curr/2]) {
		swap(arr, curr, curr/2);
		curr = curr/2;
	}
}

public static void heapifyDown(int[] arr, int size, int curr) {
	while (curr <= size/2) {
		int left = curr*2;
		int right = curr*2+1;
		
		if (right <= size && arr[right] > arr[curr] && arr[right] > arr[left]) {
			swap(arr, right, curr);
			curr = right;
		} else if (arr[left] > arr[curr]) {
			swap(arr, left, curr);
			curr = left;
		} else break;
	}
}

public static void swap(int[] arr, int i, int j) {
	int temp = arr[i];
	arr[i] = arr[j];
	arr[j] = temp;
}